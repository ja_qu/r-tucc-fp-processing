# It would be better to have an average for each degree that includes ±5deg of the data
# create the running mean for the different radi and seasons
# Radius 1000m 
  # winter
  winter_1deg_running <- MscQuanz::running_mean(data = winter_1deg)
  # intermediate
  intermediate_1deg_running <- MscQuanz::running_mean(data = intermediate_1deg)
  # summer
  summer_1deg_running <- MscQuanz::running_mean(data = summer_1deg)

# Radius 500m
  # winter
  winter_1deg_500_running <- MscQuanz::running_mean(data = winter_1deg_500)
  # intermediate
  intermediate_1deg_500_running <- MscQuanz::running_mean(data = intermediate_1deg_500)
  # summer
  summer_1deg_500_running <- MscQuanz::running_mean(data = summer_1deg_500)

#### save also the files ####
  save(summer_1deg_500_running, file = "../Results/surface/roughness/running/summer_500.RData")
  save(winter_1deg_500_running, file = "../Results/surface/roughness/running/winter_500.RData")
  save(intermediate_1deg_500_running, file = "../Results/surface/roughness/running/intermediate_500.RData")

  save(summer_1deg_running, file = "../Results/surface/roughness/running/summer_1000.RData")
  save(winter_1deg_running, file = "../Results/surface/roughness/running/winter_1000.RData")
  save(intermediate_1deg_running, file = "../Results/surface/roughness/running/intermediate_1000.RData")
  
#### Make some nice figures ####
  cairo_pdf("../Results/surface/figure/Directional_roughness_summer_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kanda 2017b including vegetation
  plotrix::radial.plot(summer_1deg_running$Zd_deg_summer, lty =1,lwd= 3, line.col = "black",
                       rp.type="p", clockwise = T,labels= paste0(seq(45,360,45),"°"), # paste0(seq(45,360,45),"°"),c("45","90","","S","SW","W","NW","N")
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,30))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35),rep(241,4),units="polar",
                     labels=c(seq(0,30,10),"zd (m)"),
                     clockwise = T, start = pi/4, col = "black", 
                     cex = 1.6)
  # z0 according to Kanda 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(summer_1deg_running$Z0_deg_summer, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5),rep(110,4),units="polar",
                     labels=c(seq(0,3,1),"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.6, radial.lim = c(0,3))
  legend(x = -3.5, y = 3.4,legend = c("zd (m)"
                                      ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  # and is this also necessary to show for the winter
  cairo_pdf("../Results/surface/figure/Directional_roughness_winter_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kanda 2017b including vegetation
  plotrix::radial.plot(winter_1deg_running$Zd_deg_winter, lty =1,lwd= 3, line.col = "black", 
                       rp.type="p", clockwise = T,labels=c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,30))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35),rep(241,4),units="polar",
                     labels=c(seq(0,30,10),"zd (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  # z0 according to Kanda 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(winter_1deg_running$Z0_deg_winter, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5),rep(110,4),units="polar",
                     labels=c(seq(0,3,1),"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2, radial.lim = c(0,3))
  legend(x = -3.5, y = 3.4,legend = c("zd (m)"
                                      ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  # I KNOW THIS IS STUPID, but also for intermediate
  # and is this also necessary to show for the winter
  cairo_pdf("../Results/surface/figure/Directional_roughness_intermediate_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kanda 2017b including vegetation
  plotrix::radial.plot(intermediate_1deg_running$Zd_deg_intermediate, lty =1,lwd= 3, 
                       line.col = "black", rp.type="p", clockwise = T,
                       labels= c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,30))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35),rep(241,4),units="polar",
                     labels=c(seq(0,30,10),"zd (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  # z0 according to Kanda 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(intermediate_1deg_running$Z0_deg_intermediate, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5),rep(110,4),units="polar",
                     labels=c(seq(0,3,1),"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2, radial.lim = c(0,3))
  legend(x = -3.5, y = 3.4,legend = c("zd (m)"
                                      ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  
  # And also for the 500 m radius
  cairo_pdf("../Results/surface/figure/Directional_roughness_summer_500_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kent 2017b including vegetation
  plotrix::radial.plot(summer_1deg_500_running$Zd_deg_summer_500, lty =1,lwd= 3, 
                       line.col = "black", rp.type="p", clockwise = T,
                       labels= c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,35))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35,40),rep(241,5),units="polar",
                     labels=c(seq(0,30,10),35,"zd (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  # z0 according to Kent 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(summer_1deg_500_running$Z0_deg_summer_500, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3.5))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5,4),rep(110,5),units="polar",
                     labels=c(seq(0,3,1),3.5,"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2, radial.lim = c(0,3))
  legend(x = -4, y = 4,legend = c("zd (m)"
                                  ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  
  # and is this also necessary to show for the winter
  cairo_pdf("../Results/surface/figure/Directional_roughness_winter_500_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kent 2017b including vegetation
  plotrix::radial.plot(winter_1deg_500_running$Zd_deg_winter_500, lty =1,lwd= 3,
                       line.col = "black", rp.type="p", clockwise = T,
                       labels= c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,35))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35,40),rep(241,5),units="polar",
                     labels=c(seq(0,30,10),35,"zd (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  # z0 according to Kent 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(winter_1deg_500_running$Z0_deg_winter_500, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3.5))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5,4),rep(110,5),units="polar",
                     labels=c(seq(0,3,1),3.5,"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  legend(x = -4, y = 4,legend = c("zd (m)"
                                  ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  ## And for the intermediate season
  # and is this also necessary to show for the winter
  cairo_pdf("../Results/surface/figure/Directional_roughness_intermediate_500_running.pdf",
            family = "Times", width=7,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kent 2017b including vegetation
  plotrix::radial.plot(intermediate_1deg_500_running$Zd_deg_intermediate_500, lty =1,lwd= 3, 
                       line.col = "black", rp.type="p", clockwise = T,
                       labels= c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels = 0, radial.lim = c(0,35))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),35,40),rep(241,5),units="polar",
                     labels=c(seq(0,30,10),35,"zd (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  # z0 according to Kent 2017b including vegetation
  par(new = T,cex.axis=1.5)
  plotrix::radial.plot(intermediate_1deg_500_running$Z0_deg_intermediate_500, lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,labels=paste0(seq(45,360,45),"°"),
              start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,3.5))
  plotrix::radial.plot.labels(lengths = c(seq(0,3,1),3.5,4),rep(110,5),units="polar",
                     labels=c(seq(0,3,1),3.5,"z0 (m)"),
                     clockwise = T, start = pi/4, col = "grey50", 
                     cex = 1.2)
  legend(x = -4, y = 4,legend = c("zd (m)"
                                  ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  
  
  
  ##### DIFFERENCE SUMMER WINTER RUNNING #####
  cairo_pdf("../Results/surface/figure/Directional_roughness_difference.pdf",
            family = "Times", width=7,height=7,bg = "white")
  # zd according to Kanda 2017b including vegetation
  par(cex.axis=1.5)
  plotrix::radial.plot(summer_1deg_running$Zd_deg_summer - winter_1deg_running$Zd_deg_winter, 
                       lty =1,lwd= 3, line.col = "black", rp.type="p", clockwise = T,
                       labels=paste0(seq(45,360,45),"°"),cex.lab = 2,
                       start = (pi/4),show.grid.labels = 0, radial.lim = c(0,5))
  plotrix::radial.plot.labels(lengths = c(seq(0,5,1),6),rep(241,4),units="polar",
                              labels=c(seq(0,5,1),"zd (m)"),
                              clockwise = T, start = pi/4, col = "grey50", 
                              cex = 1.2)
  # z0 according to Kanda 2017b including vegetation
  par(new = T)
  par(cex.axis=1.5)
  plotrix::radial.plot((summer_1deg_running$Z0_deg_summer-winter_1deg_running$Z0_deg_winter)*-1,
                       lty = 2,lwd= 3, line.col = "grey50", rp.type="p", clockwise = T,
                       labels= c("NE","E","SE","S","SW","W","NW","N"), # paste0(seq(45,360,45),"°"),
                       start = (pi/4),show.grid.labels=0, show.grid=F, radial.lim = c(0,1))
  plotrix::radial.plot.labels(lengths = c(seq(0,1,0.2),1.1),rep(110,4),units="polar",
                              labels=c(seq(0,1,0.2),"z0 (m)"),
                              clockwise = T, start = pi/4, col = "grey50", 
                              cex = 1.2, radial.lim = c(0,3))
  legend(x = -1.25, y = 1.2,legend = c("zd (m)"
                                      ,"z0 (m)"), 
         col= c("black","grey50"),lwd = 2,lty=c(1,2),bty = "n", cex= 1.5)
  dev.off()
  
  
  ##### FOR MASTER THESIS WHERE ONLY ONE ####
  
  cairo_pdf("../Results/surface/figure/Directional_zd_summer_running.pdf",
            family = "Times", width=7.5,height=7,bg = "white")
  par(cex.axis=1.5)
  # zd according to Kanda 2017b including vegetation
  plotrix::radial.plot(summer_1deg_running$Zd_deg_summer, lty =1,lwd= 5, line.col = "black", 
                       rp.type="p", clockwise = T,labels= paste0(seq(45,360,45),"°"), #  c("NE","E","SE","S","SW","W","NW","N"),
                       start = (pi/4),show.grid.labels = 0, radial.lim = c(0,30))
  plotrix::radial.plot.labels(lengths = c(seq(0,30,10),36),rep(241,4),units="polar",
                              labels=c(seq(0,30,10),"zd (m)"),
                              clockwise = T, start = pi/4, col = "black", 
                              cex = 1.5)
  legend(x = -70.5, y = 50.4,legend = c("zd (m)"), 
         col= c("black"),lwd = 2,lty=c(1),bty = "n", cex= 1.5)
  dev.off()
  