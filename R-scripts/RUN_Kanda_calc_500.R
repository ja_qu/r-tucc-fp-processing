# Make the Kanda_calculation also for the 500m radius
# As it might be better to use the 500m radius, I do the same fro the 500m radi data
# for the buildings
# (Cdv = drag coefficient for vegetation: from Hagen and skidmore 1971
# single tree and one row deciduous windbreaks 0.5, vs. coniferous windbreaks 
# 0.6-1.2)
# Kent 2017b: uses for leaf-on  P3D = 0.2 and leaf-off P3D = 0.6
# summer
pai_summer_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_500m$pai, 
                                    lambda_vegetaion = Lambda_V_500m$pai,P3D = 0.2)
pai_winter_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_500m$pai, 
                                    lambda_vegetaion = Lambda_V_500m$pai, P3D = 0.6)
pai_intermediate_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_500m$pai, 
                                    lambda_vegetaion = Lambda_V_500m$pai, P3D = 0.4)


# do the Lp_summer and winter also for each degree 
# summer
pai_deg_summer_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_1deg_500$pai, 
                                        lambda_vegetaion = Lambda_V_1deg_500$pai,P3D = 0.2)
# Winter
pai_deg_winter_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_1deg_500$pai, 
                                        lambda_vegetaion = Lambda_V_1deg_500$pai,P3D = 0.6)
pai_deg_intermediate_500 <- MscQuanz::planar_area_index(lambda_building = Lambda_B_1deg_500$pai, 
                                        lambda_vegetaion = Lambda_V_1deg_500$pai,P3D = 0.4)

# and easy calculation of the frontal area fraiton
fai_summer_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_500m$fai,
                                     fai_b = Lambda_B_500m$fai, 
                                     P3D = 0.2, Cdb = 1.2)
fai_winter_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_500m$fai,
                                     fai_b = Lambda_B_500m$fai, 
                                     P3D= 0.6, Cdb = 1.2)
fai_intermediate_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_500m$fai,
                                     fai_b = Lambda_B_500m$fai, 
                                     P3D= 0.4, Cdb = 1.2)

# and for each degree
fai_deg_summer_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_1deg_500$fai,
                                         fai_b = Lambda_B_1deg_500$fai, 
                                         P3D = 0.2, Cdb = 1.2)
fai_deg_winter_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_1deg_500$fai,
                                         fai_b = Lambda_B_1deg_500$fai, 
                                         P3D = 0.6, Cdb = 1.2)
fai_deg_intermediate_500 <- MscQuanz::frontal_area_index(fai_v = Lambda_V_1deg_500$fai,
                                         fai_b = Lambda_B_1deg_500$fai, 
                                         P3D = 0.4, Cdb = 1.2)

# Calcualte the zd and z0 from Lp_summer, and winter from Kanda:
# This should be the same as in Lambda_B_1000m$zd
# Zd_compare = Kanda_zd(pai = Lambda_B_1000m$pai,fai = Lambda_B_1000m$fai,
#                       zstd = Lambda_B_1000m$zHstd,zH = Lambda_B_1000m$zH,
#                       zHmax = Lambda_B_1000m$zHmax,Cd = 1.2)

# Zd summer lies inbetween zd_building and zd_vegetation (as it should be!)
Zd_500_summer = MscQuanz::Kanda_zd(pai = pai_summer_500, fai = fai_summer_500,
                         zH = Lambda_comb_500m$zH, zstd = Lambda_comb_500m$zHstd,
                         zHmax = Lambda_comb_500m$zHmax)

Zd_500_winter = MscQuanz::Kanda_zd(pai = pai_winter_500, fai = fai_winter_500,
                         zH = Lambda_comb_500m$zH, zstd = Lambda_comb_500m$zHstd,
                         zHmax = Lambda_comb_500m$zHmax)

Zd_500_intermediate = MscQuanz::Kanda_zd(pai = pai_intermediate_500, fai = fai_intermediate_500,
                         zH = Lambda_comb_500m$zH, zstd = Lambda_comb_500m$zHstd,
                         zHmax = Lambda_comb_500m$zHmax)
Zd_deg_summer_500 <- c()
Zd_deg_winter_500 <- c()
Zd_deg_intermediate_500 <- c()
for(i in 1:360){
  Zd_deg_summer_500[i] <- MscQuanz::Kanda_zd(pai = pai_deg_summer_500[i], 
                                   fai = fai_deg_summer_500[i],
                                   zH = Lambda_comb_1deg_500$zH[i], 
                                   zstd = Lambda_comb_1deg_500$zHstd[i],
                                   zHmax = Lambda_comb_1deg_500$zHmax[i])
  Zd_deg_winter_500[i] <- MscQuanz::Kanda_zd(pai = pai_deg_winter_500[i],
                                   fai = fai_deg_winter_500[i],
                                   zH = Lambda_comb_1deg_500$zH[i],
                                   zstd = Lambda_comb_1deg_500$zHstd[i],
                                   zHmax = Lambda_comb_1deg_500$zHmax[i])
  Zd_deg_intermediate_500[i] <- MscQuanz::Kanda_zd(pai = pai_deg_intermediate_500[i],
                                         fai = fai_deg_intermediate_500[i],
                                         zH = Lambda_comb_1deg_500$zH[i],
                                         zstd = Lambda_comb_1deg_500$zHstd[i],
                                         zHmax = Lambda_comb_1deg_500$zHmax[i])
  
}
# And the same for the z0
Z0_500_summer <- MscQuanz::Kanda_z0(fai = fai_summer_500, pai = pai_summer_500, 
                          zstd = Lambda_comb_500m$zHstd, zH = Lambda_comb_500m$zH,
                          zHmax = Lambda_comb_500m$zHmax)
Z0_500_winter <- MscQuanz::Kanda_z0(fai = fai_winter_500, pai = pai_winter_500, 
                          zstd = Lambda_comb_500m$zHstd, zH = Lambda_comb_500m$zH,
                          zHmax = Lambda_comb_500m$zHmax)
Z0_500_intermediate <- MscQuanz::Kanda_z0(fai = fai_intermediate_500, pai = pai_intermediate_500, 
                          zstd = Lambda_comb_500m$zHstd, zH = Lambda_comb_500m$zH,
                          zHmax = Lambda_comb_500m$zHmax)

Z0_deg_summer_500 <- c()
Z0_deg_winter_500 <- c()
Z0_deg_intermediate_500 <- c()
for(i in 1:360){
  Z0_deg_summer_500[i] <- MscQuanz::Kanda_z0(fai = fai_deg_summer_500[i], 
                                   pai = pai_deg_summer_500[i], 
                                   zstd = Lambda_comb_1deg_500$zHstd[i], 
                                   zH = Lambda_comb_1deg_500$zH[i],
                                   zHmax = Lambda_comb_1deg_500$zHmax[i])
  Z0_deg_winter_500[i] <- MscQuanz::Kanda_z0(fai = fai_deg_winter_500[i], 
                                   pai = pai_deg_winter_500[i], 
                                   zstd = Lambda_comb_1deg_500$zHstd[i], 
                                   zH = Lambda_comb_1deg_500$zH[i],
                                   zHmax = Lambda_comb_1deg_500$zHmax[i])
  Z0_deg_intermediate_500[i] <- MscQuanz::Kanda_z0(fai = fai_deg_intermediate_500[i], 
                                      pai = pai_deg_intermediate_500[i], 
                                      zstd = Lambda_comb_1deg_500$zHstd[i], 
                                      zH = Lambda_comb_1deg_500$zH[i],
                                      zHmax = Lambda_comb_1deg_500$zHmax[i])
}
roughnes_500m <- data.table::data.table(rbind(c(Zd_500_summer, Z0_500_summer),
                                  c(Zd_500_winter,Z0_500_winter),
                                  c(Zd_500_intermediate,Z0_500_intermediate)))
names(roughnes_500m) <- c("zd", "z0")
row.names(roughnes_500m) <- c("Summer","Winter","Intermediate")
# make data table to export
# for summer
summer_1deg_500 <- data.table::data.table(cbind(c(0:359),pai_deg_summer_500, fai_deg_summer_500, 
                                    Zd_deg_summer_500, Z0_deg_summer_500))
names(summer_1deg_500)[1] <- "Wd"
# winter
winter_1deg_500 <- data.table::data.table(cbind(c(0:359),pai_deg_winter_500, fai_deg_winter_500, 
                                    Zd_deg_winter_500, Z0_deg_winter_500))
names(winter_1deg_500)[1] <- "Wd"
# intermediate
intermediate_1deg_500 <- data.table::data.table(cbind(c(0:359),pai_deg_intermediate_500, fai_deg_intermediate_500, 
                                    Zd_deg_intermediate_500, Z0_deg_intermediate_500))
names(intermediate_1deg_500)[1] <- "Wd"

# SAVE FILES
save(roughnes_500m,file = "../Results/surface/roughness/roughnes_500m.Rdata")
save(summer_1deg_500,file = "../Results/surface/roughness/summer_roughness_1deg_500.Rdata")
save(winter_1deg_500,file = "../Results/surface/roughness/winter_roughness_1deg_500.Rdata")
save(intermediate_1deg_500,file = "../Results/surface/roughness/intermediate_roughness_1deg_500.Rdata")


#### COMPARE different RADI ####
  # plot(winter_1deg$Zd_deg_winter-winter_1deg_500$Zd_deg_winter_500, 
  #      ylab = "1000m - 500m", main = "Winter zd")
  # abline(h = 0)
  # plot(winter_1deg$Z0_deg_winter-winter_1deg_500$Z0_deg_winter_500, 
  #      ylab = "1000m - 500m", main = "winter z0")
  # abline(h = 0)
  # plot(summer_1deg$Zd_deg_summer-summer_1deg_500$Zd_deg_summer_500, 
  #      ylab = "1000m - 500m", main = "summer zd")
  # abline(h = 0)
  # plot(summer_1deg$Z0_deg_summer-summer_1deg_500$Z0_deg_summer_500, 
  #      ylab = "1000m - 500m", main = "summer z0")
  # abline(h = 0)

# There are 12 degree sectors where the calculation of Z0 fails as
# the zd is larger than the average building height in the sector
  # summer_1deg_500$Wd[which(is.na(summer_1deg_500$Z0_deg_summer_500))]
  # Lambda_comb_1deg_500$Wd[which(is.na(summer_1deg_500$Z0_deg_summer_500))]
  # wds <- which(is.na(summer_1deg_500$Z0_deg_summer_500))
  # summer_1deg_500$Zd_deg_summer_500[wds]
  # Lambda_comb_1deg_500$zHmax[wds]
  # which(Lambda_comb_1deg_500$zHmax < summer_1deg_500$Zd_deg_summer_500)
# it is not zHmax
  # fai_deg_summer_500[wds]
  # pai_deg_summer_500[wds]
  # Lambda_comb_1deg_500$zH[wds]
  # Lambda_comb_1deg_500$zHmax[wds]
  # Lambda_comb_1deg_500$zHstd[wds]
