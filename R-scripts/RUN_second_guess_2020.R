# run second guess
# if not run from RMD file, first run
# source("../R-scripts/RUN_load_surface_properties_binary.R")

first_fp_data <- read.csv("../Results/footprints/first_guess/first_fp_data.csv")
# names(first_fp_data)
years <- c("2014","2015","2016","2017","2018","2019")


second_guess_year <- list()
for(a in 1:length(years)){
  all_year_data <- which(substr(first_fp_data$TIMESTAMP, 1, 4) == years[a])
  second_guess <- data.table::data.table()
  for(i in all_year_data){ 
    if(!any(is.na(c(first_fp_data$zd_fg[i],
                    first_fp_data$wspeed[i],
                    first_fp_data$wdir[i],
                    first_fp_data$us[i],
                    first_fp_data$ol[i],
                    first_fp_data$v_sd[i])) |
            first_fp_data$perc_fg[i] < 0.8)){
      second_guess <- rbind(second_guess,
                            UCOfluxFootprint::footprint_weighted(
                                  fetch_size = 1500,
                                  zm = 56,
                                  zd = first_fp_data$zd_fg[i],
                                  wind_speed = first_fp_data$wspeed[i],
                                  wind_dir = first_fp_data$wdir[i],
                                  uStar = first_fp_data$us[i],
                                  MO_length = first_fp_data$ol[i],
                                  v_sd = first_fp_data$v_sd[i],
                                  date = first_fp_data$TIMESTAMP[i],
                                  surface_cover_info = all_raster_data,
                                  return_clip = F)) 
    }else{
      second_guess <- rbind(second_guess,
                                data.table::data.table("TIMESTAMP" = as.POSIXct(first_fp_data$TIMESTAMP[i],tz = "GMT"),
                                                       "water" = as.numeric(NA),
                                                       "bui" = as.numeric(NA),
                                                       "veg" = as.numeric(NA),
                                                       "grass" = as.numeric(NA),
                                                       "imp" = as.numeric(NA),
                                                       "perc" = as.numeric(NA),
                                                       "zd" = as.numeric(NA)))
    }
    
  }
  second_guess_year[[a]] <- second_guess
  print(years[a])
  
}


second_guess_data_all <- cbind(first_fp_data, rbind(second_guess_year[[1]], 
                               second_guess_year[[2]],second_guess_year[[3]],second_guess_year[[4]],
                               second_guess_year[[5]],second_guess_year[[6]]))

names(second_guess_data_all) <- c("timestamp..UTC.1.", "TIMESTAMP", "hfsua", 
                                  "qf_hfsua", "hflua", "qf_hflua", "H2O_sig_strgth", 
                                  "mfco2ua", "qf_mfco2ua", "CO2_sig_strgth", "t_va", 
                                  "wspeed", "wdir", "us", "ol", "zeta", "beta", "va", 
                                  "w", "ta", "hur", "rsd", "rld", "rsu", "rlu", 
                                  "prcp_rate", "rnds", "zd_wd", "v_sd", "water_fg", 
                                  "bui_fg", "veg_fg","grass_fg", "imp_fg", "perc_fg", 
                                  "zd_fg", "TIMESTAMP_new", "water_sg", "bui_sg", 
                                  "veg_sg", "grass_sg", "imp_sg", "perc_sg",  "zd_sg")
second_guess_data <- second_guess_data_all[-37]

# should be adjusted
write.csv(second_guess_data,"../Results/footprints/final_guess/second_guess_data.csv",
          row.names = F)
