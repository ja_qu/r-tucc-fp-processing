# code to calculate the new zd and z0 by each grid cell
z0_100 <- MscQuanz::create_roughness_grid(grid_bui = roughness_grid_bui,
                             grid_veg = roughness_grid_veg,
                      grid_buiveg = roughness_grid_buiveg,
                      run_zd = F,run_z0 = T,
                      cell_size = 100)
# plot(z0_100$winter, main = "z0 100m grid, winter")
zd_100 <- MscQuanz::create_roughness_grid(grid_bui = roughness_grid_bui,
                                grid_veg = roughness_grid_veg,
                                grid_buiveg = roughness_grid_buiveg,
                                run_zd = T,run_z0 = F,
                                cell_size = 100)
# plot(zd_100$winter, main = "zd 100m grid, winter")

# some problems with the 50m grid...
z0_50 <- MscQuanz::create_roughness_grid(grid_bui = roughness_grid_bui_50,
                                grid_veg = roughness_grid_veg_50,
                                grid_buiveg = roughness_grid_buiveg_50,
                                run_zd = F,run_z0 = T,
                                cell_size = 50)
# plot(z0_50$summer, main = "50m grid, winter")
zd_50 <- MscQuanz::create_roughness_grid(grid_bui = roughness_grid_bui_50,
                               grid_veg = roughness_grid_veg_50,
                               grid_buiveg = roughness_grid_buiveg_50,
                               run_zd = T,run_z0 = F,
                               cell_size = 50)
# plot(zd_50$winter, main = "50m grid, winter")


#### save the raster files ####
# crate a folder where to save
writeRaster(zd_100$winter, "../Results/surface/roughness/grid/zd100_winter",
            format = "ascii", overwrite=TRUE)
writeRaster(zd_100$intermediate, "../Results/surface/roughness/grid/zd100_intermediate",
            format = "ascii", overwrite=TRUE)
writeRaster(zd_100$summer, "../Results/surface/roughness/grid/zd100_summer",
            format = "ascii", overwrite=TRUE)

writeRaster(zd_50$winter, "../Results/surface/roughness/grid/zd50_winter",
            format = "ascii", overwrite=TRUE)
writeRaster(zd_50$intermediate, "../Results/surface/roughness/grid/zd50_intermediate",
            format = "ascii", overwrite=TRUE)
writeRaster(zd_50$summer, "../Results/surface/roughness/grid/zd50_summer",
            format = "ascii", overwrite=TRUE)

# and z0
writeRaster(z0_100$winter, "../Results/surface/roughness/grid/z0100_winter",
            format = "ascii", overwrite=TRUE)
writeRaster(z0_100$intermediate, "../Results/surface/roughness/grid/z0100_intermediate",
            format = "ascii", overwrite=TRUE)
writeRaster(z0_100$summer, "../Results/surface/roughness/grid/z0100_summer",
            format = "ascii", overwrite=TRUE)

writeRaster(z0_50$winter, "../Results/surface/roughness/grid/z050_winter",
            format = "ascii", overwrite=TRUE)
writeRaster(z0_50$intermediate, "../Results/surface/roughness/grid/z050_intermediate",
            format = "ascii", overwrite=TRUE)
writeRaster(z0_50$summer, "../Results/surface/roughness/grid/z050_summer",
            format = "ascii", overwrite=TRUE)
