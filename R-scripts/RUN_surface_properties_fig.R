# make a few figures to ilustrate the surface around the site
# crate a folder where to save
# in case not loaded:
  if(!exists("within_500m")){
    within_500m <- read.table(file = "../Results/surface/radial/within_500m.txt",
                       sep =",", header= T)
  }
  if(!exists("within_1000m")){
    within_1000m <- read.table(file = "../Results/surface/radial/within_1000m.txt",
                              sep =",", header= T)
  }
  if(!exists("within_1500m")){
    within_1500m <- read.table(file = "../Results/surface/radial/within_1500m.txt",
                               sep =",", header= T)
  }

# some impervious vaules are to high
for(i in 1:36){
  within_500m$impervious.surface.fraction.V1[i] <- (within_500m$impervious.surface.fraction.V1[i]+(1-sum(within_500m[i,])))
  within_1000m$impervious.surface.fraction.V1[i] <- (within_1000m$impervious.surface.fraction.V1[i]+(1-sum(within_1000m[i,])))
  within_1500m$impervious.surface.fraction.V1[i] <- (within_1500m$impervious.surface.fraction.V1[i]+(1-sum(within_1500m[i,])))
}


# first for th 500 m radius
cairo_pdf("../Results/surface/figure/surface_cover_barplot500.pdf",
          family = "Times", width=11,height=7,
          bg = "white")
par(family= familiy_fig, mar = c(6,4,1,2))
barplot(t(within_500m),col= c("grey30","green4","blue", "grey80"), xaxt ="n", yaxt ="n",
        space= 0, xaxs = "i")
axis(1,line = -0.3, at = seq(0.5,35.5,1),labels = c(paste0(seq(0,350,10),"°-",seq(10,360,10),"°")), las= 2)
axis(2, at= seq(0,1,0.2), las =1)
mtext(text = "surface cover fraction (-)",side = 2,line = 2.2, cex= cex_lab)
mtext(text = "Wind direction",side = 1,line = 4.5, cex= cex_lab)
legend(x = -2,y = -0.15,bty  = "n",ncol = 4,cex = 1.3,
       legend = c(expression(lambda[i]*" (-)"),
                  expression(lambda[w]*" (-)"),
                  expression(lambda[v]*" (-)"),
                  expression(lambda[b]*" (-)")),pch= 15,
       col= c("grey80","blue", "green4","grey30"), xpd =T)
abline(h= seq(0.2,0.8,0.2), col= "white", lty =3, lwd = 1.5)

dev.off()

# use the same data for the radial plot (but of course split into 360 degree)
cairo_pdf("../Results/surface/figure/surface_cover_radial500m.pdf",
          family = "Times", width=10,height=7,bg = "white")
plotrix::radial.plot(c(1,0), line.col = "grey70", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "grey70",
            start = (pi/4), show.grid.labels=1, show.grid=T)
plotrix::radial.plot(within_500m_10deg$fB+within_500m_10deg$fV+within_500m_10deg$fW, lwd= 3, line.col = "grey50", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "grey50",
            start = (pi/4), show.grid.labels=0, show.grid=T, radial.lim = c(0,1))
plotrix::radial.plot(within_500m_10deg$fW+within_500m_10deg$fV, lwd= 3, line.col = "green", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "green",
            start = (pi/4), show.grid.labels=0, show.grid=T, add = T, radial.lim = c(0,1))
plotrix::radial.plot(within_500m_10deg$fW, lwd= 3, line.col = "blue", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "blue",
            start = (pi/4), show.grid.labels=0, show.grid=T,  add = T, radial.lim = c(0,1))
plotrix::radial.plot.labels(lengths = c(0.2,0.6,0.8,1,1.1),rep(241,3),units="polar",
                   labels=c("",0.6,0.8,1," (-)"),
                   clockwise = T, start = pi/4, col = "grey50", cex = 1.4, radial.lim = c(0,1))
legend(x = -1.5, y = 1.3,legend = c(expression(lambda[p[b]]*" (-)"),
                                    expression(lambda[p[v]]*" (-)"),
                                    expression(lambda[w]*" (-)")), 
       col= c("grey50","green","blue"),pch = 15,bty = "n", cex= cex_legend)
dev.off()
# expression(lambda[p[b]]*" (-)"),
# expression(lambda[p[v]]*" (-)"),
# expression(lambda[w]*" (-)")

# c("building area fraction (-)",
#   "vegetation area fraction (-)",
#   "water area fraction (-)")

# and for the 1000m 
cairo_pdf("../Results/surface/figure/surface_cover_barplot1000.pdf",
          family = "Times", width=11,height=7,
          bg = "white")
par(family= familiy_fig, mar = c(6,4,1,2))
barplot(t(within_1000m),col= c("grey30","green4","blue", "grey80"), xaxt ="n", yaxt ="n",
        space= 0, xaxs = "i")
axis(1,line = -0.3, at = seq(0.5,35.5,1),labels = c(paste0(seq(0,350,10),"°-",seq(10,360,10),"°")), las= 2)
axis(2, at= seq(0,1,0.2), las =1)
mtext(text = "surface cover fraction (-)",side = 2,line = 2.2, cex= cex_lab)
mtext(text = "Wind direction",side = 1,line = 4.5, cex= cex_lab)
legend(x = -2,y = -0.15,bty  = "n",ncol = 4,cex = 1.3,
       legend = c(expression(lambda[i]*" (-)"),
                  expression(lambda[w]*" (-)"),
                  expression(lambda[v]*" (-)"),
                  expression(lambda[b]*" (-)")),pch= 15,
       col= c("grey80","blue", "green4","grey30"), xpd =T)
abline(h= seq(0.2,0.8,0.2), col= "white", lty =3, lwd = 1.5)
dev.off()


# and for the 1000m
cairo_pdf("../Results/surface/figure/surface_cover_radial1000.pdf",
          family = "Times", width=10,height=7,
          bg = "white")
plotrix::radial.plot(c(1,0), line.col = "grey70", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "grey70",
            start = (pi/4), show.grid.labels=0, show.grid=T)
plotrix::radial.plot(c(within_1000m_10deg$fB+within_1000m_10deg$fV+within_1000m_10deg$fW), 
            lwd= 3, line.col = "grey50", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "grey50",
            start = (pi/4), show.grid.labels=1, show.grid=T, add = T, radial.lim = c(0,1))
plotrix::radial.plot(c(within_1000m_10deg$fW+within_1000m_10deg$fV), lwd= 3, line.col = "green", 
            rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "green",
            start = (pi/4), show.grid.labels=0, show.grid=F, radial.lim = c(0,1), add = T)
plotrix::radial.plot(within_1000m_10deg$fW, lwd= 3, line.col = "blue", rp.type="p", 
            clockwise = T, labels=paste0(seq(45,360,45),"°"),poly.col = "blue",
            start = (pi/4), show.grid.labels=0, show.grid=T,  radial.lim = c(0,1),
            add = T)
plotrix::radial.plot.labels(lengths = c(0.2,0.6,0.8,1,1.1),rep(245,3),units="polar",
                   labels=c("",0.6,0.8,1," (-)"),
                   clockwise = T, start = pi/4, col = "grey50", 
                   cex = 1.4, radial.lim =c(0,1))
legend(x = -1.5, y = 1.3,legend = c(expression(lambda[p[b]]*" (-)"),
                                    expression(lambda[p[v]]*" (-)"),
                                    expression(lambda[p[w]]*" (-)")), 
       col= c("grey50","green","blue"),pch = 15,bty = "n", cex= cex_legend)
dev.off()


# and for the 1500m 
cairo_pdf("../Results/surface/figure/surface_cover_barplot1500.pdf",
          family = "Times", width=11,height=7,
          bg = "white")
par(family= familiy_fig, mar = c(6,4,1,2))
barplot(t(within_1500m_30deg),col= c("grey30","green4","blue", "grey80"), xaxt ="n", yaxt ="n",
        space= 0, xaxs = "i")
axis(1,line = -0.3, at = seq(0.5,11.5,1),
     labels = c("1°- 30°",paste0(seq(31,351,30),"°-",seq(60,360,30),"°")), las= 2)
axis(2, at= seq(0,1,0.2), las =1)
mtext(text = "surface cover fraction (-)",side = 2,line = 2.2, cex= cex_lab)
mtext(text = "Wind direction",side = 1,line = 4.5, cex= cex_lab)
legend(x = -2,y = -0.15,bty  = "n",ncol = 4,cex = 1.3,
       legend = c(expression(lambda[i]*" (-)"),
                  expression(lambda[w]*" (-)"),
                  expression(lambda[v]*" (-)"),
                  expression(lambda[b]*" (-)")),pch= 15,
       col= c("grey80","blue", "green4","grey30"), xpd =T)
abline(h= seq(0.2,0.8,0.2), col= "white", lty =3, lwd = 1.5)
dev.off()


#### NUMBERS ####
# get some numbers to describe in the text
mean(within_500m$fW[c(c(1:10),35,36)])
mean(within_500m$fW[c(9:10)])
mean(within_500m$fV[c(8:12)])
mean(within_500m$fV)
max(within_500m$fV)
which(within_500m$fV == max(within_500m$fV))
min(within_500m$fV)
which(within_500m$fV == min(within_500m$fV))
mean(within_500m$fB)
mean(within_500m$fB[c(15:23)])

mean(within_1000m$fW[c(c(1:10),35,36)])
mean(within_1000m$fV)
mean(within_1000m$fB[c(15:23)])
