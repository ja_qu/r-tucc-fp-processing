
# no caluation needed if once saved correctly
if(any(!file.exists(c("../Results/surface/grid/resampled_water.asc",
                  "../Results/surface/grid/resampled_bui.asc",
                  "../Results/surface/grid/resampled_veg.asc",
                  "../Results/surface/grid/resampled_grass.asc",
                  "../Results/surface/grid/resampled_imp.asc",
                  "../Results/surface/grid/resampled_zdsummer.asc",
                  "../Results/surface/grid/resampled_zdwinter.asc",
                  "../Results/surface/grid/resampled_zdinterm.asc")))){
  # load surface properties from 3000m with 1 and 0
  buildings_01 <- raster::raster("../data/DSM/Grid_BUI_DSM_3000_01.asc") # is 1 where is building
  crs(buildings_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  grass_01 <- raster::raster("../data/DSM/Grid_GRS_DSM_3000_01.asc") # is 1 where is grass
  crs(grass_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  vegetation_01 <- raster::raster("../data/DSM/Grid_VEG_DSM_3000_01.asc") # is 1 where is vegetation other than gras
  crs(vegetation_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  water_raster <- raster::raster("../data/DSM/Grid_WATER_MASK_3000m_Res_1m.asc")
  crs(water_raster) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
    # raster::plot(buildings_01)
  
  # load the roughness
  zd_summer_50m <- raster::raster("../Results/surface/roughness/grid/zd50_summer.asc")
  crs(zd_summer_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  zd_winter_50m <- raster::raster("../Results/surface/roughness/grid/zd50_winter.asc")
  crs(zd_winter_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  zd_intermediate_50m <- raster::raster("../Results/surface/roughness/grid/zd50_intermediate.asc")
  crs(zd_intermediate_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  
    # resample datasets to the extent of footprints
    # therefore one test footprint needs to be generated:
    # create test dataset for random period (in this case 
    # "2018-06-01 22:30:00 GMT"
    clip_test <- UCOfluxFootprint::footprint_weighted(fetch_size = 1500, zm = 56, 
                                    zd = 24.81487,
                                    wind_speed = 1.29864, 
                                    wind_dir = 208.59,
                                    uStar = 0.179658, 
                                    MO_length = -61.9702,
                                    v_sd = 0.4585128, 
                                    date = as.POSIXct("2018-06-01 22:30:00",
                                                      tz = "GMT"),
                                    return_clip = T)
    
    # resample to gereate the Imp. layer
    resample_for_imp <- raster::brick(raster::resample(water_raster, clip_test, method = "ngb"),
                                      raster::resample(buildings_01, clip_test, method = "ngb"),
                                  raster::resample(vegetation_01, clip_test, method = "ngb"),
                                  raster::resample(grass_01, clip_test, method = "ngb"),
                                  raster::resample(zd_summer_50m, clip_test, method = "ngb"),
                                  raster::resample(zd_winter_50m, clip_test, method = "ngb"),
                                  raster::resample(zd_intermediate_50m, clip_test, method = "ngb"))
    names(resample_for_imp) <- c("WAT", "BUI", "VEG", "GRS", 
                                 "zd_summer", "zd_winter", "zd_interm")
    
    # save the resampled data
    raster::writeRaster(resample_for_imp[["WAT"]], "../Results/surface/grid/resampled_water.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["BUI"]], "../Results/surface/grid/resampled_bui.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["VEG"]], "../Results/surface/grid/resampled_veg.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["GRS"]], "../Results/surface/grid/resampled_grass.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["zd_summer"]], "../Results/surface/grid/resampled_zdsummer.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["zd_winter"]], "../Results/surface/grid/resampled_zdwinter.asc",
                        format = "ascii", overwrite=TRUE)
    raster::writeRaster(resample_for_imp[["zd_interm"]], "../Results/surface/grid/resampled_zdinterm.asc",
                        format = "ascii", overwrite=TRUE)
    
    
    # calculate the impervious surface layer where all the others show zero
    imp_01 <- raster::raster(resample_for_imp[["BUI"]])
    raster::values(imp_01) <- 0
    raster::values(imp_01)[which(raster::values(resample_for_imp[["WAT"]]) == 0 &
                                raster::values(resample_for_imp[["BUI"]]) == 0 &
                                raster::values(resample_for_imp[["VEG"]]) == 0 &
                                raster::values(resample_for_imp[["GRS"]]) == 0)] <- 1
    # save the imp raster
    raster::writeRaster(imp_01, "../Results/surface/grid/resampled_imp.asc",
                        format = "ascii", overwrite=TRUE)
    
    # create the raster for analysis
    all_raster_data <- raster::brick(resample_for_imp[["WAT"]],
                                     resample_for_imp[["BUI"]],
                                     resample_for_imp[["VEG"]],
                                     resample_for_imp[["GRS"]],
                                     imp_01,
                                     resample_for_imp[["zd_summer"]],
                                     resample_for_imp[["zd_winter"]],
                                     resample_for_imp[["zd_interm"]])
    names(all_raster_data) <- c("WAT", "BUI", "VEG", "GRS", "IMP",
                                "zd_summer","zd_winter","zd_interm")
}else{
  # load surface properties from 3000m with 1 and 0
  water_01 <- raster::raster("../Results/surface/grid/resampled_water.asc") # is 1 where is building
  crs(water_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  buildings_01 <- raster::raster("../Results/surface/grid/resampled_bui.asc") # is 1 where is grass
  crs(buildings_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  vegetation_01 <- raster::raster("../Results/surface/grid/resampled_veg.asc") # is 1 where is vegetation other than gras
  crs(vegetation_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  grass_01<- raster::raster("../Results/surface/grid/resampled_grass.asc")
  crs(grass_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  imp_01 <- raster::raster("../Results/surface/grid/resampled_imp.asc")
  crs(imp_01) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  # load also the roughness resampled grids
  zd_summer_50m <- raster::raster("../Results/surface/grid/resampled_zdsummer.asc")
  crs(zd_summer_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  zd_winter_50m <- raster::raster("../Results/surface/grid/resampled_zdwinter.asc")
  crs(zd_winter_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  zd_interm_50m <- raster::raster("../Results/surface/grid/resampled_zdinterm.asc")
  crs(zd_interm_50m) <- sp::CRS("+proj=utm +zone=33 +datum=WGS84")
  # create the multible raster layer object for analysis
  all_raster_data <- raster::brick(water_01,
                                   buildings_01,
                                   vegetation_01,
                                   grass_01,
                                   imp_01,
                                   zd_summer_50m,
                                   zd_winter_50m,
                                   zd_interm_50m)
  names(all_raster_data) <- c("WAT", "BUI", "VEG", "GRS", "IMP",
                              "zd_summer", "zd_winter", "zd_interm")
}

# test <- footprint_weighted(fetch_size = 1500, zm = 56,
#                    zd = 24.81487,
#                    wind_speed = 1.29864,
#                    wind_dir = 208.59,
#                    uStar = 0.179658,
#                    MO_length = -61.9702,
#                    v_sd = 0.4585128,
#                    date = as.POSIXct("2018-06-01 22:30:00",
#                                      tz = "GMT"),
#                    surface_cover_info = all_raster_data)

