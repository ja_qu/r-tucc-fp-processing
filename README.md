# R-TUCC-FP-Processing

Project describing the processing of EddyPro Data from the TUCC Observation 
site in Berlin. The analysis and R Codes are based on the analysis applied
within my Master thesis.


1.  The processing includes first a simple description of the 
surface included in the energy exchange processes measured at the Irgason on
the main building roof top (described in FILE_XX). 

2. Using the zd values from the above described procedure (1.), a footprint 
model according to the method described by Kormann and Meixner is applied.
Also surface cover fractions are obtained from this footprint model. All results 
of this chapter will be called first guess results hereafter. All processing
steps are described in the FILE_XY.

3. As in 2. also new zd values are generated from the first guess footprints, 
these first-guess-zd values are used to run the Kormann and Meixner footprint
model a second time and obtain a better defined footprint area and thus more 
reliable information about the surface covers involved in the energy exchange.

(This description includes a very simple hypothesis, that a better/more complex 
description of the footprint area will lead to better informations about the 
surface covers involved in the energy exchange processes.)